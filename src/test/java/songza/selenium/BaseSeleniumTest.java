package songza.selenium;

import static org.junit.Assert.fail;

import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

public abstract class BaseSeleniumTest {
	
    public static final int WAIT_SECONDS = 60;
	
    // WebDriver instance - available to all page classes
    public static WebDriver webDriver = null;
    // Selenium instance - available to all page classes
    public static Selenium selenium = null;
    
    public static WebDriverWait wait = null;

    public static boolean debugMode = false;
    
    public static Properties testParams = null; // Test parameters (url, username, property ID, etc)
    
    public static ResourceBundle testData = null; // International-izable page text, control labels, etc.

    private static final String BUNDLE_NAME = "songza.selenium.test_data.test_data";
    
    /**
     * Define the setup for the tests to use.
     * 
     */
    public static void setup() throws Exception {

        // Get the test parameters from the config file.
        testParams = initTestParams();
        
        // Setup the test data source.
        testData = initTestData();
        
        // Setup the web driver backed Selenium instance.
        String browser = null;
        DesiredCapabilities capabilities = null;
        Proxy proxy = null;
        if ("true".equals(testParams.getProperty("security_testing"))) {
            proxy = new Proxy();
            proxy.setAutodetect(false);
            proxy.setProxyType(ProxyType.MANUAL);

            String proxyHost = StringUtils.trimToEmpty(testParams.getProperty("security_testing_host"));
            String proxyPort = StringUtils.trimToEmpty(testParams.getProperty("security_testing_port"));
            proxyHost = StringUtils.isNotEmpty(proxyHost) ? proxyHost : "localhost";
            proxyPort = StringUtils.isNotEmpty(proxyPort) ? proxyPort : "8090";
            proxy.setHttpProxy(proxyHost + ":" + proxyPort);
        }

        if (testParams.getProperty("selenium_version").equals("2.0")) {
            browser = testParams.getProperty("webdriverbrowser");
        }
        if (testParams.getProperty("selenium_version") == "1.0") {
            browser = testParams.getProperty("browser");
        }
        if (browser.matches("InternetExplorer")) {

            capabilities = DesiredCapabilities.internetExplorer();

            if (proxy != null) {
                capabilities.setCapability(CapabilityType.PROXY, proxy);
            }

            Properties sysProps = System.getProperties();
    		System.setProperty("webdriver.ie.driver", sysProps.getProperty("user.dir") + "//src/test/resources/ie_assets/IEDriverServer.exe");
            webDriver = new InternetExplorerDriver(capabilities);
        } else if (browser.matches("HtmlUnit")) {

            capabilities = DesiredCapabilities.firefox();
            capabilities.setJavascriptEnabled(true);

            webDriver = new HtmlUnitDriver(capabilities);
            
        } else if (browser.matches("Chrome")) {
        	// Pulls the driver for Chromedriver 2
            Properties sysProps = System.getProperties();
    		System.setProperty("webdriver.chrome.driver", sysProps.getProperty("user.dir") + "//src/test/resources/chrome_assets/chromedriver2");
    		// Starts up webDriver to use ChromeDriver
            webDriver = new ChromeDriver();
            // Maximizes the window size for Chrome Window
            chromeMaximize();

        } else if (browser.matches("Firefox")) {
            FirefoxProfile ffProfile = new FirefoxProfile();
            Properties sysProps = System.getProperties();
            String assetDir = sysProps.getProperty("user.dir") + "//src/test/resources/com/able/etw/selenium";

            capabilities = DesiredCapabilities.firefox();

            if (proxy != null) {
                capabilities.setCapability(CapabilityType.PROXY, proxy);
            }

            String baseUrl = testParams.getProperty("base_url");
            String browserFilePath = StringUtils.trimToEmpty(testParams.getProperty("browser_filepath"));
            File firefoxBinary = null;
            if (StringUtils.isNotBlank(browserFilePath)) {
                firefoxBinary = new File(browserFilePath);
            }

            // Create so we can get the version
            webDriver = new FirefoxDriver(new FirefoxBinary(firefoxBinary), ffProfile, capabilities);
            Capabilities loadedCapabilities = ((FirefoxDriver) webDriver).getCapabilities();
            String ffVersion = loadedCapabilities.getVersion();

            String[] ffVersionParts = StringUtils.split(ffVersion, ".");

            int ffVersionInt = NumberUtils.toInt(ffVersionParts[0]);
            // TODO: Maybe create a method to parse the name to get the Fire Bug
            // version
            String fireBugCurrentVersion = "1.10.6";
            String xpiName = "firebug-1.10.6.xpi";
            if (ffVersionInt >= 4 && ffVersionInt <= 5) {
                fireBugCurrentVersion = "1.7.3";
                xpiName = "firebug-1.7.3.xpi";
            }

            File fireBugXpiFile = new File(assetDir, xpiName);
            if (fireBugXpiFile.exists()) {
                ffProfile.addExtension(fireBugXpiFile);

                File firePathXpiFile = new File(assetDir, "firepath-0.9.7-fx.xpi");
                if (firePathXpiFile.exists()) {
                    ffProfile.addExtension(firePathXpiFile);
                }

            }

            // Firefox defaults
            ffProfile.setPreference("extensions.firebug.console.enableSites", Boolean.TRUE);
            ffProfile.setPreference("extensions.firebug.net.enableSites", Boolean.TRUE);
            ffProfile.setPreference("extensions.firebug.script.enableSites", Boolean.TRUE);
            ffProfile.setPreference("extensions.firebug.currentVersion", fireBugCurrentVersion);

            // Recreate with correct firebug
            webDriver.quit();
            webDriver = new FirefoxDriver(new FirefoxBinary(firefoxBinary), ffProfile, capabilities);

            selenium = new WebDriverBackedSelenium(webDriver, baseUrl);
            webDriver.manage().window().maximize();

            maximize();
        }
        String baseUrl = testParams.getProperty("base_url");
        selenium = new WebDriverBackedSelenium(webDriver, baseUrl);
        
        // Setup a default "wait" object for use in all child pages
        wait = new WebDriverWait(webDriver, WAIT_SECONDS, 500);

        // Go to the site.
        webDriver.get(baseUrl);

    }
    
    public static void maximize() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();

        double width = toolkit.getScreenSize().getWidth();
        double height = toolkit.getScreenSize().getHeight();
        org.openqa.selenium.Dimension screenResolution = new org.openqa.selenium.Dimension((int) width, (int) height);
        org.openqa.selenium.Point targetPosition = new org.openqa.selenium.Point(0, 0);
        Window window = webDriver.manage().window();
        window.setPosition(targetPosition);
        window.setSize(screenResolution);
    }

    public static void chromeMaximize() {

    	webDriver.manage().window().maximize();

    }
    
    public static void tearDown() {
    	
        if (debugMode) {
            System.out.println("Entered");
        }
    	
        // Close the application window.
        webDriver.close();
        webDriver.quit();

        if (debugMode) {
            System.out.println("Leaving");
        }

    }
    
    /**
     * Initializes the test data source.
     *
     * @return ResourceBundle that contains the test data values.
     */
    private static ResourceBundle initTestData() {

        if (debugMode) {
            System.out.println("Entered");
        }

        // Get the language and country values from the config file.
        String localeString = testParams.getProperty("locale");
        StringTokenizer st = new StringTokenizer(localeString, "-");
        String language = null;
        String country = null;
        for (int i = 1; st.hasMoreElements(); i++) {
            if (i == 1) {
                // 1st token is the language.
                language = st.nextElement().toString();
            } else {
                // 2nd token is the country.
                country = st.nextElement().toString();
            }
        }

        // Setup the locale.
        Locale currentLocale;
        currentLocale = new Locale(language, country);

        // Setup the resource bundle.
        ResourceBundle data = null;
        try {
            data = ResourceBundle.getBundle(BUNDLE_NAME, currentLocale);
        } catch (MissingResourceException mre) {
            System.out.println(">>> Cannot find resource bundle: " + BUNDLE_NAME + "_" + language + "_" + country + ".properties");
        }

        if (debugMode) {
            // String BUNDLE_NAME = "com.ca.test_data.test_data";
        	System.out.println(">>> Resource bundle is: " + BUNDLE_NAME + "_" + language + "_" + country + ".properties");
        	System.out.println("Leaving");
        }

        return data;
    }

    
    /**
     * Initializes the test parameters object.
     *
     * @return Properties object that contains the test config values.
     */
    private static Properties initTestParams() {

        Properties params = new Properties();
        Properties sysProps = System.getProperties();

        String homeDir = sysProps.getProperty("user.dir") + "//src/test/resources";
        String configFileName = sysProps.getProperty("test.config.filename");
        if (StringUtils.isEmpty(configFileName)) {
            configFileName = "test.config";
        }

        File configFile = new File(homeDir, configFileName);
        FileInputStream in = null;

        System.out.println("Entered");
        System.out.println(">>> configFile is: " + configFile.getPath());

        try {
            in = new FileInputStream(configFile);
            params.load(in);
            in.close();
        } catch (IOException e) {
        	fail("Cannot open file: test.config.");
        }

        // Display the important test parameters.
        String testParams = ">>> base_url:" + params.getProperty("base_url") + ", property_id:" + params.getProperty("property_id") + ", username:" + params.getProperty("username") + ", password:" + params.getProperty("password");
        System.out.println(testParams);
        String testParams2 = ">>> locale:" + params.getProperty("locale") + ", browser:" + params.getProperty("browser") + ", server_type:" + params.getProperty("server_type");
        System.out.println(testParams2);
        String testParams3 = ">>> security testing:" + params.getProperty("security_testing");
        System.out.println(testParams3);

        System.out.println("Leaving");

        return params;
    }

}
