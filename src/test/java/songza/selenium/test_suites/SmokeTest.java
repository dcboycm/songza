/*----------------------------------------------------------------------
 * Filename: SmokeTest.java
 *
 *
 *
 * How to add a new test class to the suite...
 *   1. Add an import statement for the new test class.
 *   2. Add an entry to the array of test classes.
 *
 */

package songza.selenium.test_suites;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import songza.selenium.BaseSeleniumTest;
import songza.selenium.test.common.BrowseTests;
import songza.selenium.test.common.EmailLoginTests;
import songza.selenium.test.common.FacebookLoginTests;
import songza.selenium.test.common.GoogleFailedLoginTests;
import songza.selenium.test.common.GoogleSuccessLoginTests;
import songza.selenium.test.common.LikedSongTests;
import songza.selenium.test.common.PlaylistRecentTests;
import songza.selenium.test.common.SkipLimitTests;
import songza.testing.annotation.type.IntegrationTest;

//Specify a runner class.
@RunWith(Suite.class)
//@formatter:off
//Specify an array of test classes.
@Suite.SuiteClasses({
		EmailLoginTests.class,
		FacebookLoginTests.class,
		GoogleFailedLoginTests.class,
		GoogleSuccessLoginTests.class,
		SkipLimitTests.class,
		LikedSongTests.class,
		PlaylistRecentTests.class,
		BrowseTests.class,
	}

)
//@formatter:on
@Category(IntegrationTest.class)

public class SmokeTest extends BaseSeleniumTest{
    @BeforeClass

    public static void suiteStart() throws Exception {
        setup();
//        login();
    }

    @AfterClass
    public static void suiteEnd() throws Exception {
        tearDown();
//         logout();
    }


}
