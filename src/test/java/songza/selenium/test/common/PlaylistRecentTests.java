package songza.selenium.test.common;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class PlaylistRecentTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Playlist Recently Played Tests
     * @throws InterruptedException 
	 *
     */
    @Test
    public void playlistRecentTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Playlist Recent Tests 1";
        String username = testData.getString("CLUB_EMAIL");
        String password = testData.getString("CLUB_PASS");
        int index = 0;
        int i = 0;
        
        System.out.println("Entered " + testId);

        // Test starts here
        // Start a playlist, check Recent's to make sure it's the song from the playlist recently played
        homePage.openHomePage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign In")));
        homePage.clickSignInButton();
        loginPage.setValueInEmailUsernameField(username);
        loginPage.setValueInPasswordField(password);
        loginPage.clickSignInWithSongzaButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Activities")));
        homePage.clickActivitiesLink();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Rainy Day")));
        homePage.clickRainyDayLink();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("browse-playlists")));
        homePage.clickBrowsePlaylistButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("player-state-play")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("miniplayer-info-track-title")));
        // this gets the first playlist title
        String playlistTitle = webDriver.findElement(By.className("browse-playlist-title")).getText();
        homePage.clickRecentsLink();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("browse-playlists")));
        assertEquals(playlistTitle, webDriver.findElement(By.className("browse-playlist-title")).getText());
        assertTrue(selenium.isTextPresent(playlistTitle));
        homePage.openHomePage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign Out")));
        homePage.clickSignOutLink();

        System.out.println("Leaving " + testId);

    }
    
}
