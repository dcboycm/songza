package songza.selenium.test.common;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.FavoritesPage;
import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class LikedSongTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	private FavoritesPage favoritesPage = new FavoritesPage();
    /**
     * Verify Liked Song Tests
     * @throws InterruptedException 
	 *
     */
    @Test
    public void likedSongTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Liked Song Tests 1";
        String username = testData.getString("CLUB_EMAIL");
        String password = testData.getString("CLUB_PASS");
        int index = 0;
        int i = 0;
        
        System.out.println("Entered " + testId);

        // Test starts here
        // Like song, check liked song list to see if it's the correct one
        homePage.openHomePage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign In")));
        homePage.clickSignInButton();
        loginPage.setValueInEmailUsernameField(username);
        loginPage.setValueInPasswordField(password);
        loginPage.clickSignInWithSongzaButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-situation-image")));
        homePage.clickConciergeSituationButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-filters")));
        homePage.clickConciergeFilterButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("player-state-play")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("miniplayer-info-track-title")));
        String songTitle = webDriver.findElement(By.className("miniplayer-info-track-title")).getText();
        homePage.clickThumbUp();
        homePage.clickFavoritesLink();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("browse-sorting-option")));
        favoritesPage.clickLikedSongsLink();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("browse-songs")));
        assertTrue(selenium.isTextPresent(songTitle));
        homePage.openHomePage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign Out")));
        homePage.clickSignOutLink();
        
        System.out.println("Leaving " + testId);

    }
    
}
