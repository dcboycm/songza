package songza.selenium.test.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class FacebookLoginTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Bad/Good Login Attempts Tests
     * @throws InterruptedException 
	 *
     */
    @Test
    public void badFacebookEmailTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Bad Facebook Email Login Test 1";
        String email = testData.getString("BAD_FACEBOOK");
        String password = testData.getString("GOOD_FACEBOOK_PASS");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sz-login-facebook")));
        loginPage.clickSignInWithFacebookButton();
        Thread.sleep(2000);
        assertEquals(true, webDriver.getWindowHandles().size() > 1);
        loginPage.switchToFacebookLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
        loginPage.setValueInFacebookEmailField(email);
        loginPage.setValueInFacebookPasswordField(password);
        loginPage.clickFacebookLoginButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("login_error_box")));
        assertTrue(selenium.isTextPresent("Incorrect Email"));
        assertTrue(selenium.isTextPresent("The email you entered does not belong to any account."));
        assertTrue(selenium.isTextPresent("You can login using any email, username or mobile phone number associated with your account. Make sure that it is typed correctly."));
        loginPage.clickBackButton();
        
        System.out.println("Leaving " + testId);

    }
    
    @Test
    public void badFacebookPasswordTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Bad Login Tests 1";
        String email = testData.getString("GOOD_FACEBOOK");
        String password = testData.getString("BAD_FACEBOOK_PASS");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sz-login-facebook")));
        loginPage.clickSignInWithFacebookButton();
        Thread.sleep(2000);
        assertEquals(true, webDriver.getWindowHandles().size() > 1);
        loginPage.switchToFacebookLoginPage();
        loginPage.setValueInFacebookEmailField(email);
        loginPage.setValueInFacebookPasswordField(password);
        loginPage.clickFacebookLoginButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("login_error_box")));
        assertTrue(selenium.isTextPresent("Please re-enter your password"));
        assertTrue(selenium.isTextPresent("The password you entered is incorrect. Please try again (make sure your caps lock is off)."));
        assertTrue(selenium.isTextPresent("Forgot your password? Request a new one."));
        loginPage.clickBackButton();
        
        System.out.println("Leaving " + testId);

    }
    
    @Test
    public void goodFacebookCredentialsTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Bad Login Tests 1";
        String email = testData.getString("GOOD_FACEBOOK");
        String password = testData.getString("GOOD_FACEBOOK_PASS");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sz-login-facebook")));
        loginPage.clickSignInWithFacebookButton();
        Thread.sleep(2000);
        assertEquals(true, webDriver.getWindowHandles().size() > 1);
        loginPage.switchToFacebookLoginPage();
        loginPage.setValueInFacebookEmailField(email);
        loginPage.setValueInFacebookPasswordField(password);
        loginPage.clickFacebookLoginButton();
        homePage.switchToSongzaHomePage();
        homePage.openHomePage();
        assertTrue(selenium.isTextPresent("Play music for"));
        homePage.clickSignOutLink();
        
        System.out.println("Leaving " + testId);

    }

}
