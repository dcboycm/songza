package songza.selenium.test.common;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class SkipLimitTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Skip Limit Tests
     * @throws InterruptedException 
	 *
     */
    @Test
    public void skipLimitTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Skip Limit - free user Tests 1";
        int index = 0;
        int i = 0;
        
        System.out.println("Entered " + testId);

        // Test starts here
        // Skip limit - free user (6) then error pops. 
        homePage.clickConciergeSituationButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-filters")));
        homePage.clickConciergeFilterButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-playlist-image-wrapper")));
        homePage.clickConciergePlaylistButton(index);
        // Need to check if the ad pops up.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-icon-ios7-fastforward")));
        while (i < 6) {
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("player-state-play")));
        	Thread.sleep(1000);
        	homePage.clickSkipButton();
        	assertFalse(selenium.isTextPresent("You've hit your skip limit!"));
        	i += 1;
		}
    	Thread.sleep(1000);
    	homePage.clickCantSkipButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("miniplayer-info-error-message")));
    	assertTrue(selenium.isTextPresent("You've hit your skip limit!"));
        homePage.openHomePage();
        homePage.clickSignOutLink();

        System.out.println("Leaving " + testId);

    }
    
    @Test
    public void skipLimitClubTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Skip Limit - Club User Tests 1";
        String username = testData.getString("CLUB_EMAIL");
        String password = testData.getString("CLUB_PASS");
        int index = 0;
        int i = 0;
        
        System.out.println("Entered " + testId);

        // Test starts here
        // club (12) qa+club@songza.com : password.
        
        homePage.openHomePage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign In")));
        homePage.clickSignInButton();
        loginPage.setValueInEmailUsernameField(username);
        loginPage.setValueInPasswordField(password);
        loginPage.clickSignInWithSongzaButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-situation")));
        homePage.clickConciergeSituationButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-filters")));
        homePage.clickConciergeFilterButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-playlist-image-wrapper")));
        // The music play automatically
//        homePage.clickConciergePlaylistButton(index);
        // Need to check if the ad pops up.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-icon-ios7-fastforward")));
        while (i < 12) {
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("player-state-play")));
        	Thread.sleep(2000);
        	System.out.println("Clicking skip button " + i);
        	homePage.clickSkipButton();
        	assertFalse(selenium.isTextPresent("You've hit your skip limit!"));
        	i += 1;
		}
    	Thread.sleep(1000);
    	homePage.clickCantSkipButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("miniplayer-info-error-message")));
    	assertTrue(selenium.isTextPresent("You've hit your skip limit!"));
        homePage.openHomePage();
        homePage.clickSignOutLink();

        System.out.println("Leaving " + testId);

    }
}
