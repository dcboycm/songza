package songza.selenium.test.common;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class GoogleFailedLoginTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Bad Login Attempts Tests
     * @throws InterruptedException 
	 *
     */
	@Test
    public void badGoogleEmailTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Bad Google Email Tests 1";
        String email = testData.getString("BAD_GOOGLE");
        String password = testData.getString("GOOD_GOOGLE_PASS");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sz-login-google")));
        loginPage.clickSignInWithGoogleButton();
        Thread.sleep(2000);
        loginPage.switchToGoogleLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Email")));
        loginPage.setValueInGoogleEmailField(email);
        loginPage.setValueInGooglePasswordField(password);
        loginPage.clickSignInButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errormsg_0_Passwd")));
        assertTrue(selenium.isTextPresent("The email or password you entered is incorrect."));
        homePage.switchToSongzaHomePage();
        loginPage.clickBackButton();

        System.out.println("Leaving " + testId);

    }

	@Test
    public void badGooglePasswordTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Bad Google Password Tests 2";
        String email = testData.getString("GOOD_GOOGLE");
        String password = testData.getString("BAD_GOOGLE_PASS");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sz-login-google")));
        loginPage.clickSignInWithGoogleButton();
        Thread.sleep(2000);
        loginPage.switchToGoogleLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Email")));
        loginPage.setValueInGoogleEmailField(email);
        loginPage.setValueInGooglePasswordField(password);
        loginPage.clickSignInButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("errormsg_0_Passwd")));
        assertTrue(selenium.isTextPresent("The email or password you entered is incorrect."));
        homePage.switchToSongzaHomePage();
        loginPage.clickBackButton();

        System.out.println("Leaving " + testId);

    }
	
}
