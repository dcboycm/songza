package songza.selenium.test.common;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class GoogleSuccessLoginTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Good Login Attempts Tests
     * @throws InterruptedException 
	 *
     */
	@Test
    public void goodGoogleCredentialsTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Bad Google Password Tests 2";
        String email = testData.getString("GOOD_GOOGLE");
        String password = testData.getString("GOOD_GOOGLE_PASS");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("sz-login-google")));
        loginPage.clickSignInWithGoogleButton();
        Thread.sleep(2000);
        loginPage.switchToGoogleLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Email")));
        loginPage.setValueInGoogleEmailField(email);
        loginPage.setValueInGooglePasswordField(password);
        loginPage.clickSignInButton();
        selenium.selectWindow(null); 
        loginPage.clickBackButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("concierge-situation-directive-muted")));
        assertTrue(selenium.isTextPresent("Play music for"));
        homePage.clickSignOutLink();
        
        System.out.println("Leaving " + testId);

    }

}
