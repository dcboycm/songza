package songza.selenium.test.common;

import static org.junit.Assert.*;

import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class BrowseTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Browse Tests
     * @throws InterruptedException 
	 *
     */
    @Test
    public void browseTest() throws InterruptedException {
    	
    	// Variables created here
        String testId = "Playlist Recent Tests 1";
        String halfA = "Something";
        String halfB = "Corporate";
        String searchString = halfA + " " + halfB;
        int index = 0;
        int i = 0;
        
        System.out.println("Entered " + testId);

        // Test starts here
        // Song start from Browse && Search - Artist, check if relative search results are present
        homePage.openHomePage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("searchbox")));
        webDriver.findElement(By.cssSelector("input[type=\"text\"]")).click();
        webDriver.findElement(By.cssSelector("input[type=\"text\"]")).clear();
        webDriver.findElement(By.cssSelector("input[type=\"text\"]")).sendKeys(searchString);
        String returnPress = Keys.chord(Keys.RETURN);
        webDriver.findElement(By.cssSelector("input[type=\"text\"]")).sendKeys(returnPress);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("search-artists")));
        assertEquals(true, webDriver.findElement(By.className("search-playlist-title")).getText().contains(halfA) || webDriver.findElement(By.className("search-playlist-title")).getText().contains(halfB));
        homePage.clickAnyString(searchString);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("browse-artist-intro")));
        assertEquals(searchString, webDriver.findElement(By.className("browse-artist-name")).getText());
        homePage.clickBrowsePlaylistButton(index);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("player-state-play")));
        homePage.openHomePage();
        
        System.out.println("Leaving " + testId);

    }
    
}
