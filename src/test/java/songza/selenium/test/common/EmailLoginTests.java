package songza.selenium.test.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import songza.page.common.HomePage;
import songza.page.common.LoginPage;
import songza.selenium.BaseSeleniumTest;

public class EmailLoginTests extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	
    /**
     * Verify Bad/Good Login Attempts Tests
	 *
     */
    @Test
    public void badUsernameTest() {
    	
    	// Variables created here
        String testId = "Bad Username Login Test 1";
        String name = testData.getString("BAD_USERNAME");
        String password = testData.getString("GOOD_PASSWORD");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("id_login-username")));
        loginPage.setValueInEmailUsernameField(name);
        loginPage.setValueInPasswordField(password);
        loginPage.clickSignInWithSongzaButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("help-block")));
        assertTrue(selenium.isTextPresent("Sorry, your username or password was incorrect."));
        loginPage.clickBackButton();
        
        System.out.println("Leaving " + testId);

    }
    
    @Test
    public void badPasswordTest() {
    	
    	// Variables created here
        String testId = "Bad Password Login Test 2";
        String name = testData.getString("GOOD_USERNAME");
        String password = testData.getString("BAD_PASSWORD");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("id_login-username")));
        loginPage.setValueInEmailUsernameField(name);
        loginPage.setValueInPasswordField(password);
        loginPage.clickSignInWithSongzaButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("help-block")));
        assertTrue(selenium.isTextPresent("Sorry, your username or password was incorrect."));
        loginPage.clickBackButton();
        
        System.out.println("Leaving " + testId);

    }
    
    @Test
    public void goodCredentialsTest() {
    	
    	// Variables created here
        String testId = "Good Credentials Login Test 3";
        String name = testData.getString("GOOD_USERNAME");
        String password = testData.getString("GOOD_PASSWORD");
        
        System.out.println("Entered " + testId);

        // Test starts here
        loginPage.openLoginPage();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("id_login-username")));
        loginPage.setValueInEmailUsernameField(name);
        loginPage.setValueInPasswordField(password);
        loginPage.clickSignInWithSongzaButton();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign Out")));
        assertTrue(selenium.isTextPresent("Sign Out"));
        assertEquals(true, webDriver.findElement(By.linkText("Sign Out")).isDisplayed());
        homePage.clickSignOutLink();
        
        System.out.println("Leaving " + testId);

    }

}
