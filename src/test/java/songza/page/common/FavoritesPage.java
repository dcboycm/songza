package songza.page.common;

import java.util.List;

import org.junit.internal.runners.statements.Fail;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class FavoritesPage extends PageParent {
	
	/**
	 * Opens the Favorites Page.
	 *
	 */
	public void open() {

        if (debugMode) {
        	System.out.println("Entered");
        }

        String favoriteUrl = "http://songza.com/user/playlists/favorites/";
        webDriver.get(favoriteUrl);

        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
	/**
	 * Click the Liked Songs link
	 * 
	 */
	public void clickLikedSongsLink() {
		
        if (debugMode) {
        	System.out.println("Entered");
        }
        
        // Verify element exists
        WebElement likedSongsLink = null;
        try {
        	likedSongsLink = webDriver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr/td[2]/div/div/div/a[2]"));
        	likedSongsLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Liked Songs link.");
		}
        
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}

}
