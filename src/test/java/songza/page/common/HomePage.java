package songza.page.common;

import java.util.List;

import org.junit.internal.runners.statements.Fail;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class HomePage extends PageParent {
	
	/**
	 * Opens the Home Page.
	 *
	 */
	public void openHomePage() {

        if (debugMode) {
        	System.out.println("Entered");
        }

        String baseUrl = testParams.getProperty("base_url");
        webDriver.get(baseUrl);

        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
    /**
     * Switches to Songza Home Page.
     *
     */
    public void switchToSongzaHomePage() {

        if (debugMode) {
			System.out.println("Entered");
        }

        // Verify the Facebook Login Page Exists and switch to it.
        try {

        	String parentHandle = webDriver.getWindowHandle(); // This gets the current window handle
        	
        	for (String winHandle : webDriver.getWindowHandles()) {
        		webDriver.switchTo().window(parentHandle); // switch focus of WebDriver to the next found window handle
        	}
        	
        } catch (NoSuchElementException e) {
            System.out.println("Songza Home Page not found");
        }
    }
    
    /**
     * Sets value in the Browse search field
     * 
     */
    public void setValueInBrowseField(String searchString) {
    	
		if (debugMode) {
			System.out.println("Entered");
		}

		// Verify the element exists
		WebElement browseField = null;
		try {
			browseField = webDriver.findElement(By.xpath("/html/body/div[3]/div[9]/input"));
			browseField.click();
			browseField.clear();
			browseField.sendKeys(searchString);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Browse Search field.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}

    }
    
    /**
     * Click the search button
     * 
     */
    public void clickSearchButton() {
    	
		if (debugMode) {
			System.out.println("Entered");
		}
    	
    	// Verify element exists
    	WebElement searchButton = null;
    	try {
			searchButton = webDriver.findElement(By.cssSelector("h3.search-artist-name"));
			searchButton.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Search Button.");
		}
    	
		if (debugMode) {
			System.out.println("Leaving");
		}

    }
    	
	/**
	 * Clicks the Songza logo - routes to songza.com
	 * 
	 */
	public void clickSongzaLogo() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement songzaLogo = null;
		try {
			songzaLogo = webDriver.findElement(By.className("ui-icon-logo"));
			songzaLogo.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Songza logo.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/*
	 * Click the Concierge Link
	 * 
	 */
	public void clickConciergeLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement concLink = null;
		try {
			concLink = webDriver.findElement(By.linkText("Concierge"));
			concLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Concierge Link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/*
	 * Click the Recents Link
	 * 
	 */
	public void clickRecentsLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement recLink = null;
		try {
			recLink = webDriver.findElement(By.linkText("Recents"));
			recLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Recents Link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}

	
	/**
	 * Clicks the Favorites link
	 * 
	 */
	public void clickFavoritesLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement favoritesLink = null;
		try {
			favoritesLink = webDriver.findElement(By.linkText("Favorites"));
			favoritesLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Favorites Link.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Clicks the Concierge Situation Buttons by Index
	 * 
	 */
	public void clickConciergeSituationButton(int index) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		List<WebElement> conButtons = null;
		try {
			conButtons = webDriver.findElements(By.className("concierge-situation"));
			conButtons.get(index).click();
			
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Concierge Situation Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}

	/**
	 * Clicks the Concierge Filter Buttons by Index
	 * 
	 */
	public void clickConciergeFilterButton(int index) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		List<WebElement> conButtons = null;
		try {
			conButtons = webDriver.findElements(By.className("concierge-filter"));
			conButtons.get(index).click();
			
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Concierge Filter Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Clicks the Concierge Playlist Buttons by Index
	 * 
	 */
	public void clickConciergePlaylistButton(int index) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		List<WebElement> conButtons = null;
		try {
			conButtons = webDriver.findElements(By.className("concierge-playlist-image"));
			conButtons.get(index).click();
			
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Concierge Playlist Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Click the Skip Button
	 * 
	 */
	public void clickSkipButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement skipBtn = null;
		try {
			skipBtn = webDriver.findElement(By.className("ui-icon-ios7-fastforward"));
			skipBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find Skip Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}

	}
	
	/**
	 * Click the Can't Skip Button
	 * 
	 */
	public void clickCantSkipButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement skipBtn = null;
		try {
			skipBtn = webDriver.findElement(By.className("ui-icon-ios7-fastforward"));
			skipBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Can't Skip Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}

	}
	
	/**
	 * Clicks the Sign In button
	 * 
	 */
	public void clickSignInButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement signInBtn = null;
		try {
			signInBtn = webDriver.findElement(By.linkText("Sign In"));
			signInBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign In Button.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Clicks the Sign Out link
	 * 
	 */
	public void clickSignOutLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement signOutLink = null;
		try {
			signOutLink = webDriver.findElement(By.linkText("Sign Out"));
			signOutLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign Out link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 *  ####        Below are the Browse Options        ####
	 */
	
	/**
	 * Clicks the Activities link
	 * 
	 */
	public void clickActivitiesLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement activitiesLink = null;
		try {
			activitiesLink = webDriver.findElement(By.linkText("Activities"));
			activitiesLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Activities link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Clicks the Rainy Day link
	 * 
	 */
	public void clickRainyDayLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement rainyDayLink = null;
		try {
			rainyDayLink = webDriver.findElement(By.linkText("Rainy Day"));
			rainyDayLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Rainy Day link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Click the browse-playlist option by index
	 * 
	 */
	public void clickBrowsePlaylistButton(int index) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		List<WebElement> browseButton = null;
		try {
			browseButton = webDriver.findElements(By.className("browse-playlist"));
			browseButton.get(index).click();
			
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the browse-playlist Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	/**
	 *  ####        Below are the Mini-Player Methods        ####
	 */
	
	/*
	 * Click the Thumbs Up - Mini-Player
	 * 
	 */
	public void clickThumbUp() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement thumbUpBtn = null;
		try {
			thumbUpBtn = webDriver.findElement(By.className("ui-icon-thumb-up"));
			thumbUpBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the thumb up button.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Click any string method
	 * 
	 */
	public void clickAnyString(String clickIt) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify element exists
		WebElement linkToClick = null;
		try {
			linkToClick = webDriver.findElement(By.linkText(clickIt));
			linkToClick.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the string " + clickIt + ".");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}

	}
	
}
