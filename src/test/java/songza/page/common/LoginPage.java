package songza.page.common;

import static org.junit.Assert.fail;

import java.util.Iterator;
import java.util.Set;

import org.junit.internal.runners.statements.Fail;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageParent {
	
	/**
	 * Opens the login Page.
	 *
	 */
	public void openLoginPage() {

        if (debugMode) {
        	System.out.println("Entered");
        }

        String baseUrl = testParams.getProperty("base_url");
        webDriver.get("http://songza.com/login/");

        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
	/**
	 * Clicks the back button
	 * 
	 */
	public void clickBackButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement backBtn = null;
		try {
			backBtn = webDriver.findElement(By.className("go-back"));
			backBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Back Button.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Clicks the Sign in with Google button
	 * 
	 */
	public void clickSignInWithGoogleButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement googleBtn = null;
		try {
			googleBtn = webDriver.findElement(By.className("sz-login-google"));
			googleBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign in with Google Button.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
    /**
     * Switches to Google Login Page.
     *
     */
    public void switchToGoogleLoginPage() {
    	
        if (debugMode) {
			System.out.println("Entered");
        }
    	
        String browser = null;
        if (testParams.getProperty("selenium_version").equals("2.0")) {
            browser = testParams.getProperty("webdriverbrowser");
        }
        if (testParams.getProperty("selenium_version") == "1.0") {
            browser = testParams.getProperty("browser");
        }

        // Verify the Google Login Page Exists and switch to it.
        try {

        	String parentHandle = webDriver.getWindowHandle(); // This gets the current window handle
        	
        	for (String winHandle : webDriver.getWindowHandles()) {
        		webDriver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle
        	}
        	
        	if (browser.matches("Firefox")) {
                webDriver.manage().window().maximize();
			} else if (browser.matches("Chrome")){
	            // Maximizes the window size for Chrome Window
	            chromeMaximize();
			}
        	
        } catch (NoSuchElementException e) {
            System.out.println("Google Login Page not found");
        }
    }
    
    /**
     * Set Value in the Google Email field
     * 
     */
    public void setValueInGoogleEmailField(String email) {
    	
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify Email field exists and fill it
		WebElement emailField = null;
		try {
			emailField = webDriver.findElement(By.id("Email"));
			emailField.clear();
			emailField.sendKeys(email);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the email field.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
    	
    }
    
    /**
     * Set Value in the Google Password field
     * 
     */
    public void setValueInGooglePasswordField(String password) {
    	
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify Password field exists and fill it
		WebElement passwdField = null;
		try {
			passwdField = webDriver.findElement(By.id("Passwd"));
			passwdField.clear();
			passwdField.sendKeys(password);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the password field.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
    	
    }
    
    /**
     * Clicks the Sign In button
     * 
     */
    public void clickSignInButton() {
    	
		if (debugMode) {
			System.out.println("Entered");
		}
		
		// Verify Sign In Button exists and click it
		WebElement signInBtn = null;
		try {
			signInBtn = webDriver.findElement(By.id("signIn"));
			signInBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign In Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}
    	
    }
	
	/**
	 * Clicks the Sign in with Facebook button
	 * 
	 */
	public void clickSignInWithFacebookButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement facebookBtn = null;
		try {
			facebookBtn = webDriver.findElement(By.className("sz-login-facebook"));
			facebookBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign in with Facebook Button.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
	}
	
    /**
     * Switches to Facebook Login Page.
     *
     */
    public void switchToFacebookLoginPage() {
    	
        if (debugMode) {
			System.out.println("Entered");
        }
    	
        String browser = null;
        if (testParams.getProperty("selenium_version").equals("2.0")) {
            browser = testParams.getProperty("webdriverbrowser");
        }
        if (testParams.getProperty("selenium_version") == "1.0") {
            browser = testParams.getProperty("browser");
        }

        // Verify the Facebook Login Page Exists and switch to it.
        try {

        	String parentHandle = webDriver.getWindowHandle(); // This gets the current window handle
        	
        	for (String winHandle : webDriver.getWindowHandles()) {
        		webDriver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle
        	}
        	
        	if (browser.matches("Firefox")) {
                webDriver.manage().window().maximize();
			} else if (browser.matches("Chrome")){
	            // Maximizes the window size for Chrome Window
	            chromeMaximize();
			}
        	
        } catch (NoSuchElementException e) {
            System.out.println("Facebook Login Page not found");
        }
    }
	
	/**
	 * Sets a string value for the email field for Facebook
	 * 
	 */
	public void setValueInFacebookEmailField(String email) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and set value in it.
		WebElement usernameField = null;
		try {
			usernameField = webDriver.findElement(By.id("email"));
			usernameField.clear();
			usernameField.sendKeys(email);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Facebook Email field.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Sets a string value for the password field for Facebook
	 * 
	 */
	public void setValueInFacebookPasswordField(String password) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and set value in it.
		WebElement usernameField = null;
		try {
			usernameField = webDriver.findElement(By.id("pass"));
			usernameField.clear();
			usernameField.sendKeys(password);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Facebook Password field.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
		
	}
	
	/**
	 * Clicks the Facebook login button
	 * 
	 */
	public void clickFacebookLoginButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement facebookLoginBtn = null;
		try {
			facebookLoginBtn = webDriver.findElement(By.id("loginbutton"));
			facebookLoginBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Facebook Login Button.");
		}

		if (debugMode) {
			System.out.println("Leaving");
		}

	}
	
	/**
	 * Sets a string value in the Email/Username field
	 * 
	 */
	public void setValueInEmailUsernameField(String username) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement usernameField = null;
		try {
			usernameField = webDriver.findElement(By.id("id_login-username"));
			usernameField.clear();
			usernameField.sendKeys(username);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Email/Username field.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
	}

	/**
	 * Sets a string value in the Password field
	 * 
	 */
	public void setValueInPasswordField(String password) {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement passwordField = null;
		try {
			passwordField = webDriver.findElement(By.id("id_login-password"));
			passwordField.clear();
			passwordField.sendKeys(password);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Password field.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
	}

	/**
	 * Clicks the Sign In with Songza button
	 * 
	 */
	public void clickSignInWithSongzaButton() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement songzaBtn = null;
		try {
			songzaBtn = webDriver.findElement(By.className("button"));
			songzaBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign In with Songza Button.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
	}
	
	/**
	 * Clicks the Sign up Link
	 * 
	 */
	public void clickSignUpLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement signUpLink = null;
		try {
			signUpLink = webDriver.findElement(By.linkText("Sign up."));
			signUpLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Sign up Link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
	}
	
	/**
	 * Clicks the Reset it Link
	 * 
	 */
	public void clickResetItLink() {
		
		if (debugMode) {
			System.out.println("Entered");
		}
		
		//Verify Element exists and click it.
		WebElement resetItLink = null;
		try {
			resetItLink = webDriver.findElement(By.linkText("Reset it."));
			resetItLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Reset it Link.");
		}
		
		if (debugMode) {
			System.out.println("Leaving");
		}
	}

}
